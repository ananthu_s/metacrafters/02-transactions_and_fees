// Import Solana web3 functinalities
const {
    Connection,
    PublicKey,
    clusterApiUrl,
    Keypair,
    LAMPORTS_PER_SOL,
    Transaction,
    SystemProgram,
    sendAndConfirmRawTransaction,
    sendAndConfirmTransaction
} = require("@solana/web3.js");

// test private key
const DEMO_FROM_SECRET_KEY = new Uint8Array(
    [
        19, 115, 127, 73, 187, 168, 217, 171, 114, 66, 40,
        128, 244, 246, 80, 30, 97, 103, 124, 26, 120, 119,
        90, 211, 147, 99, 48, 216, 196, 70, 31, 231, 118,
        79, 92, 172, 79, 107, 25, 56, 109, 171, 47, 17,
        175, 28, 41, 87, 196, 106, 255, 164, 139, 2, 200,
        155, 80, 239, 17, 246, 92, 218, 102, 52

    ]
);

const transferSol = async() => {
    const connection = new Connection(clusterApiUrl("devnet"), "confirmed");

    // Get Keypair from Secret Key
    var from = Keypair.fromSecretKey(DEMO_FROM_SECRET_KEY);

    const to = Keypair.generate();

    // Aidrop 2 SOL to Sender wallet
    console.log("Airdopping some SOL to Sender wallet!");
    const fromAirDropSignature = await connection.requestAirdrop(
        new PublicKey(from.publicKey),
        2 * LAMPORTS_PER_SOL
    );

    // Latest blockhash (unique identifer of the block) of the cluster
    let latestBlockHash = await connection.getLatestBlockhash();

    // Confirm transaction using the last valid block height (refers to its time)
    // to check for transaction expiration
    await connection.confirmTransaction({
        blockhash: latestBlockHash.blockhash,
        lastValidBlockHeight: latestBlockHash.lastValidBlockHeight,
        signature: fromAirDropSignature
    });

    console.log("Airdrop completed for the Sender account");

    const balanceLamports = await connection.getBalance(from.publicKey);
    const toLamports = await connection.getBalance(to.publicKey);

    console.log('from balance:', balanceLamports)
    console.log('to balance:', toLamports)

    // Send money from "from" wallet and into "to" wallet
    const transaction = new Transaction().add(
        SystemProgram.transfer({
            fromPubkey: from.publicKey,
            toPubkey: to.publicKey,
            lamports: balanceLamports / 2
        })
    );

    // Sign transaction
    const signature = await sendAndConfirmTransaction(
        connection,
        transaction,
        [from]
    );
    const balanceLamportsAfter = await connection.getBalance(from.publicKey);
    const toLamportsAfter = await connection.getBalance(to.publicKey);

    console.log('Signature is', signature);
    console.log('from balance:', balanceLamportsAfter)
    console.log('to balance:', toLamportsAfter)
}

transferSol();
